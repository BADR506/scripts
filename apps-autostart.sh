#!/bin/bash

if [[ -d ~/.config/autostart ]]; then
    rm -R ~/.config/autostart
    mkdir ~/.config/autostart
else
    mkdir ~/.config/autostart
fi

if [[ -f /var/lib/flatpak/exports/share/applications/ch.protonmail.protonmail-bridge.desktop ]]; then
    echo "[Desktop Entry]
Type=Application
Name=Proton Mail Bridge
Exec=/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=protonmail-bridge ch.protonmail.protonmail-bridge --no-window
Icon=ch.protonmail.protonmail-bridge" | tee ~/.config/autostart/ch.protonmail.protonmail-bridge.desktop
fi

if [[ -f /var/lib/flatpak/exports/share/applications/de.schmidhuberj.Flare.desktop ]]; then
    echo "[Desktop Entry]
Type=Application
Name=Flare
Exec=/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=/app/bin/flare de.schmidhuberj.Flare --gapplication-service
Icon=de.schmidhuberj.Flare" | tee ~/.config/autostart/de.schmidhuberj.Flare.desktop
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.gnome.Geary.desktop ]]; then
    echo "[Desktop Entry]
Type=Application
Name=Geary
Exec=/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=geary org.gnome.Geary --gapplication-service
Icon=org.gnome.Geary" | tee ~/.config/autostart/org.gnome.Geary.desktop
fi

if [[ -f /var/lib/flatpak/exports/share/applications/com.nextcloud.desktopclient.nextcloud.desktop ]]; then
	echo "[Desktop Entry]
Type=Application
Name=Nextcloud
Exec=flatpak run com.nextcloud.desktopclient.nextcloud --background
Icon=Nextcloud" | tee ~/.config/autostart/com.nextcloud.desktopclient.nextcloud.desktop
fi
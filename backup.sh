#!/bin/bash

if [[ ! -z "$(which pacman)" ]]; then
	if [[ -z "$(which hostname)" ]]; then
		sudo pacman -S inetutils
	fi
fi

if [[ -d "$HOME/Personal" ]]; then
    backup_folder="$HOME/Personal/Backup/$(hostname)"
elif [[ -d "/sd/Personal/Backup" ]]; then
    backup_folder="/sd/Personal/Backup/$(hostname)"
else
    backup_folder="$HOME/Backup/$(hostname)"
fi

if [[ -d "$backup_folder" ]]; then
    rm -rf "$backup_folder"
    mkdir -p "$backup_folder"
else
    mkdir -p "$backup_folder"
fi

if [[ -d /etc/apt/sources.list.d ]]; then
    mkdir "$backup_folder"/sources.list.d
fi

if [[ -d /etc/apt/trusted.gpg.d ]]; then
    mkdir "$backup_folder"/trusted.gpg.d
fi

if [[ -d /usr/share/keyrings ]]; then
    mkdir "$backup_folder"/keyrings
fi

if [[ ! -z "$(which apt)" ]]; then
    sudo apt autoremove

    sudo dpkg --purge $(dpkg --get-selections | grep deinstall | cut -f1) &>/dev/null
    dpkg --get-selections > "$backup_folder"/Package.list

    cp /etc/apt/sources.list "$backup_folder"/

    for file in /etc/apt/sources.list.d/*; do
        if [[ $file == *.list ]]; then
            cp -R $file "$backup_folder"/sources.list.d
        fi
    done

    if [[ -f /etc/apt/trusted.gpg ]]; then
        cp /etc/apt/trusted.gpg "$backup_folder"/
    fi

    for file in /etc/apt/trusted.gpg.d/*; do
        if [[ $file == *.gpg ]]; then
            cp -R $file "$backup_folder"/trusted.gpg.d
        fi
    done

    for file in /usr/share/keyrings/*; do
        if [[ $file == *.gpg ]]; then
            cp -R $file "$backup_folder"/keyrings
        fi
    done
fi

if [[ ! -z "$(which pacman)" ]]; then
    pacman -Qdtq | sudo pacman --noconfirm -Rs -

    pacman -Qqen > "$backup_folder"/pkglist.txt

    pacman -Qqem > "$backup_folder"/localpkglist.txt

    pacman -Q > "$backup_folder"/packages.txt

    sudo pacman --noconfirm -Sc

    cp /etc/pacman.conf "$backup_folder"/
fi

if [[ ! -z "$(which snap)" ]]; then
    snap list | tail -n +2 | sed 's/\ .*//' | tee /tmp/snap_names.txt &>/dev/null
    snap list | tail -n +2 | sed 's/.*latest\///' | sed 's/\ .*//' | tee /tmp/snap_tracking.txt &>/dev/null
    
    paste -d "\ " /tmp/snap_names.txt /tmp/snap_tracking.txt | tee "$backup_folder"/snap.txt &>/dev/null

    rm /tmp/snap_names.txt
    rm /tmp/snap_tracking.txt
fi

if [[ ! -z "$(which flatpak)" ]]; then
    flatpak uninstall --unused
    
    flatpak list --columns=origin,ref | tail -n +1 > "$backup_folder"/flatpak.txt
fi

if [[ ! -z "$(which pip)" ]]; then
    pip freeze | tee "$backup_folder"/pip.txt
fi

if [[ ! -z "$(which pip3)" ]]; then
    pip3 freeze | tee "$backup_folder"/pip3.txt
fi

dconf dump / > "$backup_folder"/dconf
#!/bin/bash

Real_Path()
{
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#.}"
}

if [[ -f ~/.profile ]]; then
    profile=".profile"
else
    profile=".bashrc"
fi

if [[ ! -f ~/"$profile" && -f /etc/skel/"$profile" ]]; then
    cp /etc/skel/"$profile" ~/
fi

if [[ ! -f ~/.bash_logout && -f /etc/skel/.bash_logout ]]; then
    cp /etc/skel/.bash_logout ~/
fi

if [[ ! -f ~/.bash_profile && -f /etc/skel/.bash_profile ]]; then
    cp /etc/skel/.bash_profile ~/
fi

if [[ -z $(grep "PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '" ~/"$profile") ]]; then
	echo "PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '" | tee -a ~/"$profile"
fi

if [[ -z $(grep "HISTSIZE" ~/"$profile") ]]; then
    echo "HISTSIZE=
HISTFILESIZE=
stty -ixon" | tee -a ~/"$profile"
fi

if [[ -z $(grep "export SCRIPTSPATH=" ~/"$profile") ]]; then
    echo "export SCRIPTSPATH=$(echo $(Real_Path "${0%/*}") | sed "s|$HOME|\$HOME|")
export PATH=\$SCRIPTSPATH:\$PATH
chmod +x \$SCRIPTSPATH/*.sh" | tee -a ~/"$profile"
fi

source ~/"$profile"
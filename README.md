# My Scripts

## What are these scripts for?
These are scripts created for and by me. I'm experienced in bash scripting and using it for such purposes on macOS and Linux. I'm the creator of open source bash scripts and of these open source scripts.

## Licensing information
All scripts in this repository have been written by me and are under the GPL-3.0 license that I have chosen to use.

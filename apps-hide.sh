#!/bin/bash

declare -a keep_apps=("im.riot.Riot")

if [[ -d ~/.local/share/applications ]]; then
    for keep_app in ${keep_apps[@]}; do 
        if [[ -f ~/.local/share/applications/$keep_app.desktop ]]; then
            echo "Keeping $keep_app"

            mv ~/.local/share/applications/$keep_app.desktop ~/.local/share
        fi
    done

    for file in ~/.local/share/applications/*; do
        if [[ $file == *org.gnome.Epiphany.WebApp_*.desktop ]]; then
            mv $file ~/.local/share/
        fi
    done

    rm -R ~/.local/share/applications
    mkdir ~/.local/share/applications

    for keep_app in ${keep_apps[@]}; do 
        if [[ -f ~/.local/share/$keep_app.desktop ]]; then
            mv ~/.local/share/$keep_app.desktop ~/.local/share/applications
        fi
    done

    for file in ~/.local/share/*; do
        if [[ $file == *org.gnome.Epiphany.WebApp_*.desktop ]]; then
            echo "Keeping $(basename $file)"

            mv $file ~/.local/share/applications/
        fi
    done
else
    mkdir ~/.local/share/applications
fi

declare -a apps=("avahi-discover" "bssh" "bvnc" "qv4l2" "qvidcap" "system-config-printer" "nm-connection-editor" "vim" "syncthing-start" "syncthing-ui")

for app in ${apps[@]}; do 
    if [[ -f /usr/share/applications/$app.desktop ]]; then
        echo "Hiding $app"

        cp /usr/share/applications/$app.desktop ~/.local/share/applications

        echo "NoDisplay=true" | tee -a ~/.local/share/applications/$app.desktop
    fi
done
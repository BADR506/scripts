#!/bin/bash

loadkeys fr_CH-latin1

read -e -p "[install-arch.sh] user: " user

read -e -p "[install-arch.sh] hostname: " hostname

read -e -p "[install-arch.sh] lockscreen message: " message

timedatectl set-ntp true

cryptsetup luksFormat /dev/sda2

cryptsetup open /dev/sda2 cryptlvm

pvcreate /dev/mapper/cryptlvm

vgcreate vgroot /dev/mapper/cryptlvm

lvcreate -L 10G vgroot -n swap
lvcreate -l 100%FREE vgroot -n root

mkfs.ext4 /dev/vgroot/root
mkswap /dev/vgroot/swap

mount /dev/vgroot/root /mnt
swapon /dev/vgroot/swap

mkfs.fat -F32 /dev/sda1

mkdir /mnt/boot

mount /dev/sda1 /mnt/boot

pacman -Sy archlinux-keyring --noconfirm

pacstrap /mnt base linux linux-firmware

genfstab -U /mnt >> /mnt/etc/fstab

arch-chroot /mnt pacman-key --init

arch-chroot /mnt pacman-key --populate 

arch-chroot /mnt pacman -S --noconfirm base-devel cups evolution-data-server flatpak gdm ghostscript gnome-backgrounds gnome-console gnome-control-center gnome-disk-utility gnome-shell gnome-software gnome-system-monitor gst-plugin-pipewire gst-plugins-good gvfs-goa ipp-usb lvm2 nautilus networkmanager nss-mdns pipewire-alsa pipewire-pulse power-profiles-daemon reflector sane-airscan sudo system-config-printer wayland xdg-user-dirs

arch-chroot /mnt flatpak config --set extra-languages "en_GB;fr_CH;de_CH"

arch-chroot /mnt flatpak install --noninteractive org.gnome.baobab org.gnome.Calculator org.gnome.Calendar org.gnome.Characters org.gnome.clocks org.gnome.Contacts org.gnome.eog org.gnome.Epiphany org.gnome.Evince org.gnome.font-viewer org.gnome.Logs org.gnome.NautilusPreviewer org.gnome.Maps org.gnome.Music org.gnome.Photos org.gnome.SimpleScan org.gnome.SoundRecorder org.gnome.TextEditor org.gnome.Totem org.gnome.Weather

arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime

arch-chroot /mnt hwclock --systohc

echo "en_GB.UTF-8 UTF-8" | tee -a /mnt/etc/locale.gen
echo "fr_CH.UTF-8 UTF-8" | tee -a /mnt/etc/locale.gen
echo "de_CH.UTF-8 UTF-8" | tee -a /mnt/etc/locale.gen

arch-chroot /mnt locale-gen

echo "LANG=en_GB.UTF-8" | tee /mnt/etc/locale.conf

echo "KEYMAP=fr_CH-latin1" | tee /mnt/etc/vconsole.conf

echo "$hostname" | tee /mnt/etc/hostname

echo "127.0.0.1 localhost" | tee -a /mnt/etc/hosts
echo "::1		localhost" | tee -a /mnt/etc/hosts
echo "127.0.1.1 $hostname" | tee -a /mnt/etc/hosts

arch-chroot /mnt bootctl --path=/boot/ install

echo "default arch
timeout 3
editor 0" | tee /mnt/boot/loader/loader.conf

echo "title Arch Linux
linux /vmlinuz-linux
initrd /initramfs-linux.img
options cryptdevice=UUID=$(blkid /dev/sda2 | sed 's/.*\ UUID="//' | sed 's/".*//'):cryptlvm root=/dev/vgroot/root rw" | tee /mnt/boot/loader/entries/arch.conf

echo "title Arch Linux fallback
linux /vmlinuz-linux
initrd /initramfs-linux-fallback.img
options cryptdevice=UUID=$(blkid /dev/sda2 | sed 's/.*\ UUID="//' | sed 's/".*//'):cryptlvm root=/dev/vgroot/root rw" | tee /mnt/boot/loader/entries/arch-fallback.conf

sed -i 's/HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block filesystems fsck)/HOOKS=(base udev autodetect modconf kms block keyboard encrypt lvm2 filesystems fsck)/' /mnt/etc/mkinitcpio.conf

arch-chroot /mnt mkinitcpio -P

echo "%wheel ALL=(ALL) ALL" | tee -a /mnt/etc/sudoers

arch-chroot /mnt useradd -m -G wheel -s /bin/bash $user

arch-chroot /mnt passwd $user

arch-chroot /mnt systemctl enable avahi-daemon.service cups.service cups-browsed.service fstrim.timer gdm.service NetworkManager.service reflector.service systemd-boot-update.service

echo "polkit.addRule(function(action, subject) {
    if (action.id == \"org.opensuse.cupspkhelper.mechanism.all-edit\" &&
        subject.isInGroup(\"wheel\")){
        return polkit.Result.YES;
    }
});" | tee /mnt/etc/polkit-1/rules.d/49-allow-passwordless-printer-admin.rules

sed -i 's/resolve/mdns_minimal [NOTFOUND=return] resolve/' /mnt/etc/nsswitch.conf

echo b08dfa6083e7567a1921a715000001fb | tee /mnt/var/lib/dbus/machine-id 

echo b08dfa6083e7567a1921a715000001fb | tee /mnt/etc/machine-id

echo "sudo machinectl shell gdm@ /bin/bash -c \"gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true\"

sudo machinectl shell gdm@ /bin/bash -c \"gsettings set org.gnome.login-screen banner-message-enable true\"

sudo machinectl shell gdm@ /bin/bash -c \"gsettings set org.gnome.login-screen banner-message-text '$message'\"" | tee /mnt/home/$user/arch-setup.sh

if [[ $(arch-chroot /mnt bootctl status | grep "Secure Boot" | sed 's/.*(//' | sed 's/)//') == "setup" ]]; then
    echo "echo \"[package-repo]
SigLevel = Optional TrustAll
Server = https://julianfairfax.gitlab.io/package-repo/\" | sudo tee -a /etc/pacman.conf

curl -s https://julianfairfax.gitlab.io/package-repo/pub.gpg | sudo pacman-key --add -
sudo pacman-key --lsign-key C123CB2B21B9F68C80A03AE005B2039A85E7C70A

sudo pacman -S sbkeys sbupdate

sudo sh -c 'cd /etc/efi-keys && sbkeys'
echo \"CMDLINE_DEFAULT=\\\"cryptdevice=UUID=$(sudo blkid /dev/sda2 | sed 's/.*\ UUID="//' | sed 's/".*//'):cryptlvm root=/dev/vgroot/root\\\"
SPLASH=\\\"/dev/null\\\"
EXTRA_SIGN=(\\\"/boot/EFI/BOOT/BOOTX64.EFI\\\" \\\"/boot/EFI/systemd/systemd-bootx64.efi\\\")\" | sudo tee -a /etc/sbupdate.conf
sudo sbupdate 
    
sudo mkdir -p /etc/secureboot/keys/{db,dbx,KEK,PK}
sudo cp /etc/efi-keys/DB.auth /etc/secureboot/keys/db
sudo cp /etc/efi-keys/KEK.auth /etc/secureboot/keys/KEK
sudo cp /etc/efi-keys/PK.auth /etc/secureboot/keys/PK
    
sudo sbkeysync --verbose
sudo sbkeysync --verbose --pk

echo \"efi /EFI/Arch/linux-signed.efi\" | sudo tee -a /boot/loader/entries/arch.conf
echo \"efi /EFI/Arch/linux-signed.efi\" | sudo tee -a /boot/loader/entries/arch-fallback.conf" | tee -a /mnt/home/$user/arch-setup.sh
fi

chmod +x /mnt/home/$user/arch-setup.sh
arch-chroot /mnt chown $user:$user /home/$user/arch-setup.sh
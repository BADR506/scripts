#!/bin/bash

if [[ ! -z "$(which pacman)" ]]; then
	if [[ -z "$(which hostname)" ]]; then
		sudo pacman -S inetutils
	fi
fi

if [[ -d "$HOME/Personal" ]]; then
    backup_folder="$HOME/Personal/Backup/$(hostname)"
elif [[ -d "/sd/Personal/Backup" ]]; then
    backup_folder="/sd/Personal/Backup/$(hostname)"
else
    backup_folder="$HOME/Backup/$(hostname)"
fi

if [[ ! -z "$(which apt)" ]]; then
    sudo cp "$backup_folder"/sources.list /etc/apt/

    for file in "$backup_folder"/sources.list.d/*; do
        if [[ $file == *.list ]]; then
            sudo cp -R $file /etc/apt/sources.list.d
        fi
    done

    sudo cp "$backup_folder"/trusted.gpg /etc/apt/

    for file in "$backup_folder"/trusted.gpg.d/*; do
        if [[ $file == *.gpg ]]; then
            sudo cp -R $file /etc/apt/trusted.gpg.d
        fi
    done

    for file in "$backup_folder"/keyrings/*; do
        if [[ $file == *.gpg ]]; then
            sudo cp -R $file /usr/share/keyrings
        fi
    done

    export DEBIAN_FRONTEND=noninteractive

    sudo apt update
    sudo apt install dselect -y

    sudo apt-cache dumpavail > /tmp/temp_avail
    sudo dpkg --merge-avail /tmp/temp_avail

    sudo rm /tmp/temp_avail

    sudo dpkg --set-selections < "$backup_folder"/Package.list
    sudo apt-get dselect-upgrade
fi

if [[ ! -z "$(which pacman)" ]]; then
	sudo cp "$backup_folder"/pacman.conf /etc/

    sudo pacman -S --needed $(comm -12 <(pacman -Slq|sort) <(sort "$backup_folder"/pkglist.txt) )

    sudo pacman -Syy

    pacman -Qdtq | sudo pacman --noconfirm -Rs -

    sudo pacman --noconfirm -Sc
fi

if [[ ! -z "$(which snap)" ]]; then
    while read snap; do
        echo "snap install "$(echo "$snap" | sed 's/\ /\ --/')"" | sudo /bin/bash
    done < "$backup_folder"/snap.txt
fi

if [[ ! -z "$(which flatpak)" ]]; then
    while read origin pkg; do
        flatpak install --assumeyes $origin $pkg
    done < "$backup_folder"/flatpak.txt
fi

if [[ ! -z "$(which pip)" ]]; then
    pip install -r "$backup_folder"/pip.txt
fi

if [[ ! -z "$(which pip3)" ]]; then
    if [[ ! -z $(diff "$backup_folder"/pip.txt "$backup_folder"/pip3.txt) ]]; then
        pip3 install -r "$backup_folder"/pip3.txt
    fi
fi
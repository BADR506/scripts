#!/bin/bash

if [[ ! -d ~/.local/share/applications ]]; then
    mkdir ~/.local/share/applications
fi

if [[ ! -d ~/.config/autostart ]]; then
    mkdir ~/.config/autostart
fi


if [[ -f /var/lib/flatpak/exports/share/applications/ch.protonmail.protonmail-bridge.desktop ]]; then
    flatpak override --user --env=QT_QPA_PLATFORM=wayland ch.protonmail.protonmail-bridge --socket=wayland --socket=session-bus
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.mozilla.firefox.desktop ]]; then
    flatpak override --user --env=MOZ_ENABLE_WAYLAND=1 org.mozilla.firefox --socket=wayland
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.nicotine_plus.Nicotine.desktop ]]; then
	flatpak override --user --env=NICOTINE_GTK_VERSION=4 --env=NICOTINE_LIBADWAITA=1 org.nicotine_plus.Nicotine
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.signal.Signal.desktop ]]; then
    flatpak override --user --env=SIGNAL_USE_WAYLAND=1 org.signal.Signal --socket=wayland
fi


if [[ -f /var/lib/flatpak/exports/share/applications/im.riot.Riot.desktop ]]; then
    cp /var/lib/flatpak/exports/share/applications/im.riot.Riot.desktop ~/.local/share/applications
    sed -i 's/@@u %U @@/@@u %U @@ --enable-features=UseOzonePlatform,WaylandWindowDecorations --ozone-platform=wayland/' ~/.local/share/applications/im.riot.Riot.desktop
fi


if [[ -f /etc/xdg/autostart/org.gnome.Software.desktop ]]; then
    cp /etc/xdg/autostart/org.gnome.Software.desktop ~/.config/autostart
    echo "Icon=org.gnome.Software
Hidden=true" | tee -a ~/.config/autostart/org.gnome.Software.desktop
fi

if [[ -f /var/lib/flatpak/exports/share/applications/org.gnome.Music.desktop ]]; then
	flatpak override --user --unshare=network org.gnome.Music
fi